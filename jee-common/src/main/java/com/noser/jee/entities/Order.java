package com.noser.jee.entities;


import javax.persistence.*;

import java.util.List;

import static javax.persistence.FetchType.EAGER;

@Entity
@NamedQueries({
    @NamedQuery(name = "Order.findOne", query = "select c from Order c where c.id = :id"),
    @NamedQuery(name = "Order.getAll", query = "select c from Order c")
})
public class Order {

  @Id
  @GeneratedValue
  public long id;

  @ManyToMany(fetch = EAGER)
  public List<Article> articles;

  public Order() {
  }

  public Order(List<Article> articles) {
    this.articles = articles;
  }

}