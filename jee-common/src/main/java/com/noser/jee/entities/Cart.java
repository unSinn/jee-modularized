package com.noser.jee.entities;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.FetchType.EAGER;

@Entity
@NamedQueries({
    @NamedQuery(name = "Cart.findOne", query = "select c from Cart c where c.id = :id"),
    @NamedQuery(name = "Cart.getAll", query = "select c from Cart c")
})
public class Cart {

  @Id
  @GeneratedValue
  public long id;

  @ManyToMany(fetch = EAGER)
  public List<Article> articles;

  public Cart() {
  }

  public Cart(List<Article> articles) {
    this.articles = articles;
  }

}