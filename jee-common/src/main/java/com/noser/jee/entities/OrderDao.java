package com.noser.jee.entities;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class OrderDao {

  @PersistenceContext
  private EntityManager em;

  public List<Order> getAll() {
    return em.createNamedQuery("Order.getAll", Order.class).getResultList();
  }

  public Order find(Long id) {
    return em.createNamedQuery("Order.findOne", Order.class).setParameter("id", id).getSingleResult();
  }

  public void save(Order a) {
    em.persist(a);
  }

  public void update(Order a) {
    em.merge(a);
  }

  public void delete(Order a) {
    em.remove(a);
  }
}
