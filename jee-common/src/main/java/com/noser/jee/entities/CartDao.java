package com.noser.jee.entities;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class CartDao {

  @PersistenceContext
  private EntityManager em;

  public List<Cart> getAll() {
    return em.createNamedQuery("Cart.getAll", Cart.class).getResultList();
  }

  public Cart find(Long id) {
    return em.createNamedQuery("Cart.findOne", Cart.class).setParameter("id", id).getSingleResult();
  }

  public void save(Cart a) {
    em.persist(a);
  }

  public void update(Cart a) {
    em.merge(a);
  }

  public void delete(Cart a) {
    em.remove(a);
  }
}
