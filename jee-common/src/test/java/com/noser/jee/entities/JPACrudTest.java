package com.noser.jee.entities;

import org.junit.Test;

import static java.util.Collections.emptyList;
import static org.junit.Assert.assertNotNull;

public class JPACrudTest extends JPAHibernateTest {
  @Test
  public void createArticleTest() {
    Order a = new Order();
    a.articles = emptyList();

    em.persist(a);

    Order b = em.find(Order.class, 1L);

    assertNotNull(b);
  }

}
