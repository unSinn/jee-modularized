package com.noser.jeeshop.cart.boundary;

import com.noser.jee.entities.Article;
import com.noser.jee.entities.ArticleDao;
import com.noser.jee.entities.Cart;
import com.noser.jee.entities.CartDao;
import com.noser.jeeshop.cart.entity.AddCart;
import io.swagger.annotations.Api;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Api(value = "Carts service")
@Path("carts")
public class Carts {

  @Inject
  private Logger logger;

  @Inject
  CartDao carts;

  @Inject
  ArticleDao articles;

  @GET
  public List<Cart> getCarts() {
    logger.info("Sending all Carts");
    return carts.getAll();
  }

  @POST
  public void add(AddCart a) {
    logger.info("Got {}", a);
    List<Article> articles = a.articleIds.stream().map(id -> this.articles.find(id)).collect(toList());
    Cart cart = new Cart(articles);
    carts.save(cart);
  }

}
