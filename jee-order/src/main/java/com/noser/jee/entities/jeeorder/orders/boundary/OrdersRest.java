package com.noser.jee.entities.jeeorder.orders.boundary;

import com.noser.jee.entities.Order;
import io.swagger.annotations.Api;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.List;

@Api(value = "OrdersRest service")
@Path("orders")
public class OrdersRest {

  @Inject
  private Logger logger;

  @Inject
  private OrdersCatalog ordersCatalog;

  @GET
  public List<Order> getOrders() {
    logger.info("Sending all OrdersRest");
    return ordersCatalog.getAll();
  }

}
