package com.noser.jee.entities.jeeorder.orders.boundary;

import com.noser.jee.entities.Order;
import com.noser.jee.entities.OrderDao;
import org.eclipse.microprofile.metrics.annotation.Gauge;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.atomic.LongAdder;

@ApplicationScoped
public class OrdersCatalog {

  private LongAdder requests;

  @Inject
  OrderDao orders;

  @PostConstruct
  public void init() {
    requests = new LongAdder();
  }

  List<Order> getAll() {
    requests.increment();
    return orders.getAll();
  }

  @Gauge(unit = "count")
  public int numberOfRequests() {
    return requests.intValue();
  }

}
