# Build
mvn clean package && docker build -t com.noser/jeeorder .

# RUN

docker rm -f jeeorder || true && docker run -d -p 8080:8080 -p 4848:4848 --name jeeorder com.noser/jeeorder

# OpenAPI
http://localhost:8080/jeeorder/api/apiee/index.html#/ 